﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace CrapsApplication
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sBJSearchMasterDataSet.zProfiles' table. You can move, or remove it, as needed.
            this.zProfilesTableAdapter.Fill(this.sBJSearchMasterDataSet.zProfiles);

            int profileIdInt;
            if (cmbProfiles.SelectedValue != null && int.TryParse(cmbProfiles.SelectedValue.ToString(), out profileIdInt))
            {
                txtEditName.Text = cmbProfiles.Text;
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text))
            {
                lblMessage.Text = "Please enter a profile name";
            }
            else
            {
                SqlConnection conn = new SqlConnection(Globals.connString);
                SqlCommand cmd = new SqlCommand("insert into zProfiles ([name]) values (@name); select SCOPE_IDENTITY();", conn);
                cmd.Parameters.AddWithValue("@name", txtName.Text);

                try
                {
                    conn.Open();
                    var newIdObject = cmd.ExecuteScalar();
                    int newId;
                    if (newIdObject != null && int.TryParse(newIdObject.ToString(), out newId))
                    {
                        Globals.profile_id = newId;
                        Globals.profile_name = lblMessage.Text;

                        Globals.CreateGame(conn);

                        Form1 form1 = new Form1();
                        form1.Show();

                        this.Hide();
                    }
                }
                catch
                {
                    lblMessage.Text = "Error adding profile";
                }
                finally
                {
                    conn.Dispose();
                }
            }
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            int profileIdInt;
            if (cmbProfiles.SelectedValue != null && int.TryParse(cmbProfiles.SelectedValue.ToString(), out profileIdInt))
            {
                Globals.profile_id = profileIdInt;
                Globals.profile_name = cmbProfiles.Text;

                Globals.CreateGame(new SqlConnection(Globals.connString));

                Form2 thisForm = new Form2();
                Form1 form1 = new Form1();

                form1.Show();
                this.Hide();
            }
            else
            {
                lblMessage.Text = "Please select a profile or create a new one";
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int profileIdInt;
            if (cmbProfiles.SelectedValue != null && int.TryParse(cmbProfiles.SelectedValue.ToString(), out profileIdInt))
            {
                SqlConnection conn = new SqlConnection(Globals.connString);
                SqlCommand cmd = new SqlCommand("update zProfiles set [name] = @name where profile_id = @id", conn);
                cmd.Parameters.AddWithValue("@name", txtEditName.Text);
                cmd.Parameters.AddWithValue("@id", profileIdInt);

                try
                {
                    conn.Open();
                    cmd.ExecuteScalar();
                    Form2_Load(sender, e);
                }
                catch { }
                finally
                {
                    conn.Dispose();
                }
            }
        }

        private void cmbProfiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            int profileIdInt;
            if (cmbProfiles.SelectedValue != null && int.TryParse(cmbProfiles.SelectedValue.ToString(), out profileIdInt))
            {
                txtEditName.Text = cmbProfiles.Text;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
