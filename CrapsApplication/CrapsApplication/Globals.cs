﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CrapsApplication
{
    public static class Globals
    {
        public static int profile_id;
        public static string profile_name;
        public static int game_id;

        public static string connString = ConfigurationManager.ConnectionStrings["DatabaseConnString"].ConnectionString;

        public static void CreateGame(SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand("insert into zGames (profile_id) values (@profile); select SCOPE_IDENTITY();", conn);
            cmd.Parameters.AddWithValue("@profile", profile_id);

            try
            {
                conn.Open();
                var newIdObject = cmd.ExecuteScalar();
                int newId;
                if (newIdObject != null && int.TryParse(newIdObject.ToString(), out newId))
                {
                    Globals.game_id = newId;
                }
            }
            catch
            {

            }
            finally
            {
                conn.Dispose();
            }
        }

        public static void UpdateGame(SqlConnection conn, string result)
        {
            SqlCommand cmd = new SqlCommand("update zGames set result = @result where game_id = @id", conn);
            cmd.Parameters.AddWithValue("@result", result);
            cmd.Parameters.AddWithValue("@id", game_id);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch { }
            finally
            {
                conn.Dispose();
            }
        }
    }
}
