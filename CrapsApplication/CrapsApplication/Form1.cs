﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CrapsApplication
{
    public partial class Form1 : Form
    {
        private int rollNumber = 0;
        private int pointNumber = 0;

        private static Random rnd = new Random();
        private string winResult = "You rolled a {0} and have won!";
        private string loseResult = "You rolled a {0} and have lost...";
        private string pointResult = "You rolled a {0}, it is now your point.";
        private string contResult = "You rolled a {0}, keep rolling.";

        private static int GetResult()
        {
            return rnd.Next(1, 6);
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void btnDie1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Globals.connString);
            rollNumber += 1;

            var die1 = GetResult();
            var die2 = GetResult();
            var diceSum = die1 + die2;

            if (rollNumber == 1)
            {
                if (diceSum == 2 || diceSum == 3 || diceSum == 12)
                {
                    lblResult.Text = string.Format(loseResult, diceSum);
                    Globals.UpdateGame(conn, lblResult.Text);
                    ResetGame();
                }
                else if (diceSum == 7 || diceSum == 11)
                {
                    lblResult.Text = string.Format(winResult, diceSum);
                    Globals.UpdateGame(conn, lblResult.Text);
                    ResetGame();
                }
                else
                {
                    pointNumber = diceSum;
                    lblResult.Text = string.Format(pointResult, diceSum);
                }
            }
            else
            {
                if (diceSum == 7)
                {
                    lblResult.Text = string.Format(loseResult, diceSum);
                    Globals.UpdateGame(conn, lblResult.Text);
                    ResetGame();
                }
                else if (diceSum == pointNumber)
                {
                    lblResult.Text = string.Format(winResult, diceSum);
                    Globals.UpdateGame(conn, lblResult.Text);
                    ResetGame();
                }
                else
                {
                    lblResult.Text = string.Format(contResult, diceSum);
                }
            }

            EnterResult(diceSum);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void ResetGame()
        {
            txtDie1.Text = "";
            txtDie2.Text = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblProfileName.Text = string.Format("Current profile: {0}", Globals.profile_name);
        }

        private void EnterResult(int diceSum)
        {
            SqlConnection conn = new SqlConnection(Globals.connString);
            SqlCommand cmd = new SqlCommand("insert into zRolls (roll, game_id) values (@roll, @game)", conn);
            cmd.Parameters.AddWithValue("@roll", Globals.game_id);
            cmd.Parameters.AddWithValue("@game", diceSum);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Dispose();
            }
        }
    }
}
