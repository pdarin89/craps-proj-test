﻿namespace CrapsApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDie1 = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtDie2 = new System.Windows.Forms.TextBox();
            this.txtDie1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblProfileName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnDie1
            // 
            this.btnDie1.Location = new System.Drawing.Point(129, 114);
            this.btnDie1.Name = "btnDie1";
            this.btnDie1.Size = new System.Drawing.Size(75, 23);
            this.btnDie1.TabIndex = 0;
            this.btnDie1.Text = "Roll";
            this.btnDie1.UseVisualStyleBackColor = true;
            this.btnDie1.Click += new System.EventHandler(this.btnDie1_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(129, 143);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtDie2
            // 
            this.txtDie2.Location = new System.Drawing.Point(104, 67);
            this.txtDie2.Name = "txtDie2";
            this.txtDie2.Size = new System.Drawing.Size(100, 20);
            this.txtDie2.TabIndex = 2;
            // 
            // txtDie1
            // 
            this.txtDie1.Location = new System.Drawing.Point(104, 41);
            this.txtDie1.Name = "txtDie1";
            this.txtDie1.Size = new System.Drawing.Size(100, 20);
            this.txtDie1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Die 1 result:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Die 2 result:";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(35, 194);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 13);
            this.lblResult.TabIndex = 6;
            // 
            // lblProfileName
            // 
            this.lblProfileName.AutoSize = true;
            this.lblProfileName.Location = new System.Drawing.Point(63, 9);
            this.lblProfileName.Name = "lblProfileName";
            this.lblProfileName.Size = new System.Drawing.Size(0, 13);
            this.lblProfileName.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.lblProfileName);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDie1);
            this.Controls.Add(this.txtDie2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDie1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDie1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtDie2;
        private System.Windows.Forms.TextBox txtDie1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblProfileName;
    }
}

